import setuptools

setuptools.setup(
    name='Stand off data',
    author='Patrick Jentsch',
    author_email='p.jentsch@uni-bielefeld.de',
    description='A python library to handle stand off data.',
    py_modules=['stand_off_data'],
    classifiers=[
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.5'
)
