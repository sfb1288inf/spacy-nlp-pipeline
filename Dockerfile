FROM debian:buster-slim


LABEL authors="Patrick Jentsch <p.jentsch@uni-bielefeld.de>"


ARG UID="1000"
ARG GID="1000"


ENV LANG="C.UTF-8"
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONUNBUFFERED="1"


RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
      python \
      python-virtualenv \
      python3 \
      python3-venv \
      wget \
 && rm --recursive /var/lib/apt/lists/*


RUN groupadd --gid "${GID}" generic \
 && useradd --create-home --gid generic --no-log-init --uid "${UID}" generic
USER generic
WORKDIR /home/generic


ENV PYTHON2_VENV_PATH="/home/generic/python2_venv"
ENV PYTHON3_VENV_PATH="/home/generic/python3_venv"
RUN python2 -m virtualenv "${PYTHON2_VENV_PATH}" \
 && python3 -m venv "${PYTHON3_VENV_PATH}"
ENV PATH="${PYTHON3_VENV_PATH}/bin:${PYTHON2_VENV_PATH}/bin:${PATH}"


ENV PYFLOW_VERSION="1.1.20"
RUN wget \
      --output-document - \
      --quiet "https://github.com/Illumina/pyflow/releases/download/v${PYFLOW_VERSION}/pyflow-${PYFLOW_VERSION}.tar.gz" \
    | tar -xzf - \
 && cd "pyflow-${PYFLOW_VERSION}" \
 && python2 setup.py build install \
 && cd - > /dev/null \
 && rm --recursive "pyflow-${PYFLOW_VERSION}"


COPY --chown=generic:generic packages packages
COPY --chown=generic:generic requirements.txt .
RUN python3 -m pip install --upgrade pip \
 && python3 -m pip install wheel \
 && python3 -m pip install --requirement requirements.txt \
 && rm --recursive packages requirements.txt


COPY spacy-models spacy-nlp spacy-nlp-pipeline vrt-creator /usr/local/bin/


ENTRYPOINT ["spacy-nlp-pipeline"]
CMD ["--help"]
